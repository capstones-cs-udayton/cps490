# Capstone II Proposal 

University of Dayton

Department of Computer Science

CPS 490 - Fall 2022

+ Dr. Nick Stiffler (2022 - )
+ Dr. Phu Phung (2018 - 2021)


# Project Topic


# Team members



1.  Member 1, email
2.  Member 2, email
3.  Member 3, email
4.  Member 4, email


# Company Mentors

Mentor 1, _title_; 

Mentor 2, _title_; ... 

Company name

Company address

## Project homepage

[Provide the URL to your team project homepage here]


# Overview

Describe the overview of the project with a high-level architecture figure (See an example below).


![Overview Architecture](https://capstones-cs-udayton.bitbucket.io/imgs/overview-sample.png "A Sample of Overview Architecture")

Figure 1. - A Sample of Overview Architecture of the proposed project.

# Project Context and Scope

Describe the context where the project will be used or deployed, i.e., the motivation of your project, and the scope of the project your team will develop. 
Discuss the interaction of the expected system in your project with people and the physical world.


# High-level Requirements

List high-level requirements of the project that your team will develop into use cases in later steps


# Technology

Specify the development approach of your team, including programming languages, database, development, testing, and deployment environments. 

You also need to mention if there are any technical support and preferences from the company and if they provide an existing version of the application.

# Impacts

Describe impacts of the technologies that your team decided you use in this project, i.e., the arguments that your team use these technologies.


# Project Management

We will follow the Scrum approach, thus your team needs to identify the task in each sprint cycles, team meeting schedules, including this Fall and next Spring semester. The tenative schedule and sprint cycles for Spring 2022 are as follows. 

![Spring 2022 Timeline](https://capstones-cs-udayton.bitbucket.io/imgs/cps491timeline.png "Spring 2022 Timeline")


In this section, provide the link to your team bitbucket repository and Trello board. Your team must share the repo and the board with nstiffler1@udayton.edu

Include the screenshot of the timeline from your Trello board (with tasks). You can use the Trello template available here (only with timeline): [https://trello.com/b/uIgKfjr6/cps491-s21](https://trello.com/b/uIgKfjr6/cps491-s21)  


Below is the example of the Trello board timeline (Gantt chart) with sprint cycles but without tasks for Spring 2022: 

![Spring 2022 Timeline on Trello](https://capstones-cs-udayton.bitbucket.io/imgs/trello.png "Spring 2022 Timeline")



# Company Support

Based on your discussion with the company, describe here what they will support your team during this Fall and next Spring semester, and the communication plan with the company mentors. 
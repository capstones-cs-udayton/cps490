# CPS 490 Capstone I

## Instructor(s)

+ Dr. Nick Stiffler (2022 - )
+ Dr. Phu Phung (2018 - 2021)


# Sprint 3 - Messenger v0.3

Total points: 100 

Latest update: 2022-11-01

## Introduction 

In this sprint, your team will continue revising the current version of your
*Messenger* application and add more functionality.  The outcome of this sprint
is a working prototype with several selected requirements from users together
with the current documentation about the analysis, design, implementation, and
evaluation.

## Preparation

Before starting this sprint, your team needs to review all the code to merge the `sprint2` branch into the `master` branch.
Similar to Sprint2, your team needs to create a “`sprint3`” branch.

For this assignment, your team need to create “sprint2” branch and organize the branch as follows.

*   `README.md`: the main documentation of this sprint report.
*   “`docs`”: a sub-folder to save other documents and the demo screenshots 
*   “`src`”:  a sub-folder to save the source code of your implementation.


## REQUIREMENTS AND GRADE DISTRIBUTION



1. Implementation: 65 points

    1.1. Functional Requirements
        
        1.1.1. (10 points) All messages, including public, private, group chats, will be stored in the database
        
        1.1.2. (10 points) Logged-in users can view chat history from public, private, group chats after logging out and logging in again

        1.1.3. (5 points) Logged-in users can edit their account with additional information such as Full Name, email, phone number, etc
        
        1.1.4. (5 points) Users can create and keep connections with other users (i.e. ``Friend'' other users)

        1.1.5. (10 points) Two additional use cases of your team's choice

    1.2. Non-functional requirements
    
        1.2.1. The system must be secure and defend against simple web attacks 
               
               - (2.5 points) All data must be validated in all layers before sending/checking/forwarding
               
               - (2.5 points) All data must be sanitized client-side before displaying

        1.2.2. (2.5 points) Passwords must be hashed in the database
        
        1.2.3. Deployment with DevOps CI/CD

               - (2.5 points) Dockerfile for the application
               
               - (5 points) Setup a pipeline for continuous deployment

    1.3. Repository 
    
        1.3.1. (2.5 points) Clean organization
        
        1.3.2. (2.5 points) No temporary and/or designated should-be-ignored files/folders such as node_modules 
        
        1.3.3. (5 points) Pushing on the branch, create a pull request, and merge to the master branch when the sprint is completed


2. Documentation: 25 points (same as in Sprint 1 and 2). 

    2.1. Requirements:
    
        2.1.1. (3 points) Format: Use the provided README.md template from Sprint 0, with the following information: the course and instructor, the project and sprint information, your team members and IDs. The project description should include Introduction, Analysis, Design, Implementation, and Evaluation and Progress Report. The progress report should mention **how many hours each team member contributed, explanation of roles for each team member** in this sprint. You need to provide **the link to the latest commit** for the sprint in this section.
        
        2.1.2. (1 point) Source code in a single separate PDF file. 

    2.2. Analysis
    
        2.2.1. (1 point) [Updated] Requirements, General User Case Diagram
        
        2.2.2. (5 points) [Additional] Each use case: User Story, Brief Use Case Description, and Fully Developed Use Case Description or System Sequence Diagram or Activity Diagram  

    2.3. Design
 
        2.3.1. (2 points) [Updated] The general architecture of the system
        
        2.3.2. (3 points) [Additional] Sequence (Interaction) diagrams for implemented use cases.
        
        2.3.3. (5 points) [New] Database Design, E-R Diagram is preferred
        
    2.4. Implementation
       
        2.4.1. (2.5 points) [Additional]Code snippets with the explanation of each use case implemented  

    2.5. Evaluation

        2.5.1. (2.5 points) [Additional] Screenshots of working use cases with a caption


3. Presentation: 10 points

    3.1. (5 points) Slides: Proper slides preparation (In Google Slides, send the link to the instructor before the presentation time)

    3.2. (5 points) Communication skills: Appropriate presentation 


## SUBMISSION

Submit the following files in Isidore by the deadline. One submission per team.

1. `sprint3-team#-report.pdf` (# is your team number) converted from README.md 

2. Source code in `sprint3-team#-source-code.pdf`

3. Presentation slides in `sprint3-team#-slides.pdf`


## CPS 490 Capstone I
### Department of Computer Science, University of Dayton
#### Instructor(s): 
- Dr. Nick Stiffler (2022 - )
- Dr. Phu Phung (2018 - 2021)

### Team project
# Messenger Web Application 

## Final version (Sprint 3)
* Total points: 100 (20% of final grade)
* Sprint planned start date: 10/29
* Sprint introduction: 11/02 (Lecture 15)
* Sprint planned end date: 12/02
* Presentations: 12/02
* Submission deadline: 12/02, 3:35 PM, accept until 12/04 11:55 PM  

## Introduction 

In this final sprint, your team will continue revising your current prototype to add more functionalities. 
If your team did not complete any functionalities from previous sprints, your team needs to complete all by the end of this sprint.
As a final stage of this project, the outcome of this sprint is a complete working prototype and final documentation about the analysis, design, implementation, and evaluation.

## Preparation

Before starting this sprint, your team needs to review all the code to merge the "sprint2" branch to the master branch.
Similar to previous sprints, your team needs to create “`sprint3`” branch. 
To reduce the possible conflicts among members, it is highly recommended each member create a new branch within the new “`sprint3`” branch and continue to merge to “`sprint3`” branch when a task is completed.
As a reminder, never commit the code in the master branch as it will be difficult to resolve the conflicts.

## REQUIREMENTS AND GRADE DISTRIBUTION


1. Implementation: 65 points

    1.1. Functional Requirements
        
        1.1.1. (10 points) All messages, including public, private, group chats, will be stored in the database
        
        1.1.2. (10 points) Logged-in users can view chat history from public, private, group chats after logging out and login again

        1.1.3. (5 points) Logged-in users can edit their account with additional information such as Full Name, Phone Number, etc
        
        1.1.4. (5 points) Users can create and keep connections with other users, e.g., friendships

        1.1.5. (10 points) Two addition requirements from your team
    
    1.2. Non-functional requirements
    
        1.2.1. The system must be secure and defend against simple web attacks 
               
               - (2.5 points) All data must be validated in all layers before sending/checking/forwarding
               
               - (2.5 points) All data must be sanitized in client-side before displaying

        1.2.2. (2.5 points) Passwords must be hashed in the database
        
        1.2.3. Deployment with DevOps CI/CD

               - (2.5 points) Dockerfile for the application
               
               - (5 points) Setup a pipeline for continuous deployment

    1.3. Repository 
    
        1.3.1. (2.5 points) Clean organization
        
        1.3.2. (2.5 points) No temporary and should-be-ignored files/folders such as node_modules 
        
        1.3.3. (5 points) Pushing on the branch, create a pull request, and merge to the master branch when the sprint is completed
        

2. Documentation: 25 points (same as in Sprint 1 and 2). 

    2.1. Requirements:
    
        2.1.1. (3 points) Format: Use the provided README.md template from Sprint 0, with the following information: the course and instructor, the project and sprint information, your team members and IDs. The project description should include Introduction, Analysis, Design, Implementation, and Evaluation and Progress Report. The progress report should mention **how many hours each team member contributed, explanation of roles for each team member** in this sprint. You need to provide **the link to the latest commit** for the sprint in this section.
        
        2.1.2. (1 point) Source code in a single separate PDF file. 

    2.2. Analysis
    
        2.2.1. (1 point) [Updated] Requirements, General User Case Diagram
        
        2.2.2. (5 points) [Additional] Each use case: User Story, Brief Use Case Description, and Fully Developed Use Case Description or System Sequence Diagram or Activity Diagram  

    2.3. Design
 
        2.3.1. (2 points) [Updated] The general architecture of the system
        
        2.3.2. (3 points) [Additional] Sequence (Interaction) diagrams for implemented use cases.
        
        2.3.3. (5 points) [New] Database Design, E-R Diagram is preferred
        
    2.4. Implementation
       
        2.4.1. (2.5 points) [Additional]Code snippets with the explanation of each use case implemented  

    2.5. Evaluation

        2.5.1. (2.5 points) [Additional] Screenshots of working use cases with a caption


3. Presentation: 10 points
    
    3.1. (5 points) Slides: Proper slides preparation (In Google Slides, send the link to the instructor before the presentation time)
    
    3.2. (5 points) Communication skills: Appropriate presentation 


## SUBMISSION

Submit the following files in Isidore by the deadline. One submission per team.

1. `project-final-team#-report.pdf` (# is your team number) converted from your README.md file. 

2. Source code in `project-final-team#-source-code.pdf`. Alternatively, your team can submit separate source files in .txt

3. Presentation slides in `project-final-team#-slides.pdf`


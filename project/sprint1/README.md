# CPS 490 Capstone I

## Instructor(s)

+ Dr. Nick Stiffler (2022 - )
+ Dr. Phu Phung (2018 - 2021)


# Sprint 1 - Messenger v0.1

Total points: 100 

Latest update: 2022-09-01

## Introduction 

This is the first sprint in your team project. The outcome of this sprint is a working prototype with several selected requirements from users together with the current documentation about the analysis, design, implementation, and evaluation.

## Preparation

This is a team assignment, thus, you need to save all the code/documentation in your team repository. Similar in the Lab 1, your team repo MUST have the “`.gitignore`” file to untrack the temporary files/folders. 

For this assignment, your team need to create “`sprint1`” branch and organize the branch as follows.

*   `README.md`: the main documentation of this sprint report.
*   “`docs`”: a sub-folder to save other documents and the demo screenshots 
*   “`src`”:  a sub-folder to save the source code of your implementation.



## REQUIREMENTS AND GRADE DISTRIBUTION


1. Implementation: 70 points

    1.1. User Interface (UI)

        1.1.1. (5 points) Separation between login and chat UI
        
        1.1.2. (5 points) Different UIs for public chat, and private chat
        
        1.1.3. (5 points) With CSS, e.g., use an external library such as Bootstrap, jQuery ...

    1.2. Front-end 
    
        1.2.1. (5 points) Display/send messages and real-time typing status for public chat 
        
        1.2.2. (5 points) Display/send messages and real-time typing status for private chat
        
        1.2.3. (5 points) Maintain the userlist
   
    1.3. Back-end
   
        1.3.1. (5 points) Essential requirements: Simple login with username, logged-in users can send and receive public messages
        
        1.3.2. (10 points) Sprint requirements: Logged-in users can send/receive to/from a specific online user
        
        1.3.3. (10 points) Your team choice: Pick up three or more requirements in identified Sprint 0 and implement them. 
    
    1.4. Repository 
    
        1.4.1. (2.5 points) Clean organization
        
        1.4.2. (2.5 points) No temporary and should-be-ignored files/folders such as node_modules 
        
        1.4.3. (5 points) Pushing on the branch, create a pull request, and merge to the master branch when the sprint is completed

    1.5 Deployment
        (5 points) The application is successfully deployed on the cloud and public accessible 


2. Documentation: 20 points. 

    2.1. Requirements:
    
        2.1.1. (3 points) Format: Use the provided README.md template from Sprint 0, with the following information: the course and instructor, the project and sprint information, your team members and IDs. The project description should include Introduction, Analysis, Design, Implementation, and Evaluation and Progress Report. The progress report should mention **how many hours each team member contributed, explanation of roles for each team member** in this sprint. You need to provide **the link to the latest commit** for the sprint in this section.
        
        2.1.2. (1 point) Source code in a separate PDF file. 

    2.2. Analysis
    
        2.2.1. (1 point) Requirements, General User Case Diagram
        
        2.2.2. (5 points) Each use case: User Story, Brief Use Case Description, and Fully Developed Use Case Description or System Sequence Diagram or Activity Diagram  

    2.3. Design
 
        2.3.1. (2 points) The general architecture of the system
        
        2.3.2. (3 points) Sequence (Interaction) diagrams for implemented use cases.
        
    2.4. Implementation
       
        2.4.1. (2.5 points) Code snippets with the explanation of each use case implemented  

    2.5. Evaluation

        2.5.1. (2.5 points) Screenshots of working use cases with a caption


3. Presentation: 10 points

    3.1. (5 points) Slides: Proper slides preparation (In Google Slides, send the link to the instructor before the presentation time)

    3.2. (5 points) Communication skills: Appropriate presentation 


## SUBMISSION

Submit the following files in Isidore by the deadline. One submission per team.

1. `sprint1-team#-report.pdf` (# is your team number) converted from README.md 

2. Source code in `sprint1-team#-source-code.pdf`

3. Presentation slides in `sprint1-team#-slides.pdf`